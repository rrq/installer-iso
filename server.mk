# Include file for building an install ISO, aka server ISO

# These define where on the ${ISOTREE} the kernel and initrd are placed
INITRD = /boot/isolinux/initrd.gz
KERNEL = /boot/isolinux/linux

IS_INSTALLER = full_cd

# Choice of boot options
include boot-bios-isolinux.mk
include boot-efi-grub.mk
include win32-loader.mk

# Choice of package collection
POOL += INSTALL

# Choice of firmware collection
FIRMWARE += ${ISOTREE}/firmware

# Combine udeb sets into the choice collection, stripped of commented lines
UDEBSELECT = kernel base
