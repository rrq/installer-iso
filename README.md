Devuan Installer ISO
====================

This project is tailored for building the Devuan installation ISOs,
which includes architectures i386 and amd4 with a collection of ISO
for each:
* netinstall - installation fully over the network
* server - ~CD size ISO for server installation, also without network
* desktop - 4G ISO for desktop installation also without network
* cd2 - package pool desktop add-on for server install, with xfce4 and 
* cd3 - package pool desktop add-on for server install, with ...
* pool1 - package pool of the 5000 top votes from devuan popcon
* pool2 - package pool of the 15000 top votes from devuan popcon

The `pool` Directory
--------------------

This directory contains "seed" files and a Makefile for creating the
package lists for the various ISOs. Use "make -C pool reallyclean" to
clean it up.

The pool directory in particular includes a local "library" of package
description snippets, which are held in the library-$ARCH-$SECTION
directories. The snippets are obtained from the build host's Packages
files by breaking them up into individual package descriptions named
by the package name. The build host must therefore be set up with the
target sources and it must be duly updated for the target ISO
building.

The local library uses several optional source points that are
overlayed on a simple assumption of version priority. Overall it uses
the following source list point, where the first is required, and the
rest are optional.

 1. $CODENAME main main/debian-installer contrib non-free
 3. $CODENAME-security main main/debian-installer
 4. $CODENAME-updates main main/debian-installer
 5. $CODENAME-proposed-updates main main/debian-installer

The pool direcory also keeps the package list files that are used for
populating the ISO. The first ISO building step includes the
processing of the seed files to generate the actual, complete package
list for the target ISO by recursively following up on all
dependencies and recommended packages.

How to Build an ISO
-------------------

ISOs are only built for the current (aka host) architecture. Use

    $ ./build-sudo.sh $SUITE $ISO $VERSION

with

* ISO being one of netinstall, server, desktop, cd2, cd3, pool1 or
  pool2, or the special token all to build all of them.

* SUITE being one of ascii, beowulf, chimaera or daedalus

* VERSION being the accompanying release version code, e.g. 3.a for
  beowulf. This version code gets imprinted into the ISO.

Help?
-----

On occasion, things don't just work. This typically means that the
package collection (which is downloaded on demand) has changed in some
way that results in an inconsistency between package descriptions and
downloaded packages. The master switch for recovery is then a
`reallyclean` of the build area, and an `update + dist-upgrade` of the
build host, possibly followed by a `reboot`, esp if the kernel version
has changed. For example:

    $ make reallyclean
    $ ./build-sudo.sh beowulf netinstall 3.a

