pipeline {

    agent {label 'master'}

    options {
	buildDiscarder logRotator(numToKeepStr: '3')
    }

    environment {
	SUITENAME = 'beowulf'
    }

    stages {
	stage('Architecture Matrix') {
            matrix {
		agent {label  "isos && ${architecture}"}
		environment {
	            NFS = "/nfs/${architecture}-builder/isobuild/${SUITENAME}-chroot"
		}
                axes {
                    axis {
                        name 'architecture'
			values 'amd64', 'i386'
                    }
                }
                stages {
                    stage('Create Chroot') {
			environment {
			    INCLUDE = "build-essential,newlisp,isolinux,syslinux-common,syslinux-utils,fakeroot,libbogl-dev,bf-utf-source,grub-efi-${architecture == 'i386' ? 'ia32' : architecture}-bin,grub-pc,dosfstools,mtools,win32-loader,tofrodos,xorriso,git,ca-certificates,zsh,make,wget"
			    XINCLUDE = "build-essential newlisp isolinux syslinux-common syslinux-utils fakeroot libbogl-dev bf-utf-source grub-efi-${architecture == 'i386' ? 'ia32' : architecture}-bin grub-pc dosfstools mtools win32-loader tofrodos xorriso git ca-certificates zsh make wget"
			}
                        steps {
                            /* ircNotify customMessage: "$BUILD_DISPLAY_NAME: started binary build for ${architecture} on ${NODE_NAME}" */
			    sh "rm -rf ${NFS}"

			    sh "fakechroot -e debootstrap fakeroot debootstrap --include=${INCLUDE} --variant=fakechroot --no-merged-usr --arch ${architecture}  ${SUITENAME} ${NFS} http://pkgmaster.devuan.org/merged || true"

			    /* PATCH 2: grup-pc requires a succeeding /systemd-detect-virt script */
			    sh "echo 'touch /tmp/PATCH2' > ${NFS}/systemd-detect-virt"
			    sh "chmod a+x ${NFS}/systemd-detect-virt"
			    sh "fakechroot -e debootstrap fakeroot chroot ${NFS} chown root. /systemd-detect-virt"
			    /* end PATCH 2 */

			    sh "fakechroot -e debootstrap fakeroot chroot ${NFS} dpkg --configure -a"
			    sh "fakechroot -e debootstrap fakeroot chroot ${NFS} apt-get -y install devuan-keyring"
			    sh "fakechroot -e debootstrap fakeroot chroot ${NFS} apt-get -y install ${XINCLUDE}"
			}
		    }
		    stage ( 'Add Source to Chroot' ) {
		        steps {
			    /*
			     Set up to build on NFS = /nfs/${architecture}-builder/isobuild
			     ${NFS} = chroot environment for building
			     ${NFS}/installer-iso = rsynced from ${WORKSPACE}
			     */
			    sh "mkdir ${NFS}/installer-iso"
			    sh "rsync -av ${WORKSPACE}/. ${NFS}/installer-iso/."
                        }
                    }
		    stage('Configure Chroot') {
			steps {
			    writeFile file: "${NFS}/etc/apt/sources.list", text: """
deb http://pkgmaster.devuan.org/merged ${SUITENAME} main contrib non-free
deb-src http://pkgmaster.devuan.org/merged ${SUITENAME} main contrib non-free
deb http://pkgmaster.devuan.org/merged ${SUITENAME} main/debian-installer
deb http://pkgmaster.devuan.org/merged ${SUITENAME}-security main contrib non-free
deb-src http://pkgmaster.devuan.org/merged ${SUITENAME}-security main contrib non-free
deb http://pkgmaster.devuan.org/merged ${SUITENAME}-security main/debian-installer
deb http://pkgmaster.devuan.org/devuan ${SUITENAME}-updates main contrib non-free
deb-src http://pkgmaster.devuan.org/devuan ${SUITENAME}-updates main contrib non-free
deb http://pkgmaster.devuan.org/devuan ${SUITENAME}-updates main/debian-installer
deb http://pkgmaster.devuan.org/devuan ${SUITENAME}-proposed-updates main contrib non-free
deb-src http://pkgmaster.devuan.org/devuan ${SUITENAME}-proposed-updates main contrib non-free
deb http://pkgmaster.devuan.org/devuan ${SUITENAME}-proposed-updates main/debian-installer
"""
			    writeFile file: "${NFS}/etc/apt/apt.conf.d/05norecommends", text: 'APT::Install-Recommends "0";'
			}
		    }
		    stage('Update Chroot') {
			steps {
			    sh "fakechroot -e debootstrap fakeroot chroot ${NFS} apt-get -y update"
			    sh "fakechroot -e debootstrap fakeroot chroot ${NFS} apt-get -y dist-upgrade"

			    /* initramfs-tools is broken with fakechroot
			    environment (see https://bugs.debian.org/944929), so
			    use tiny-initramfs instead */
			    sh "fakechroot -e debootstrap fakeroot chroot ${NFS} apt-get -y install -t ${SUITENAME} linux-image-${architecture == 'i386' ? '686' : architecture} kmod tiny-initramfs"

			}
		    }
		    stage('Build') {
			steps {
			    sh "fakechroot -e debootstrap chroot ${NFS} installer-iso/runbuild.sh"
			}
		    }
		    stage('Archive') {
			steps {
			    sh "rsync -av ${NFS}/installer-iso/*.iso ${NFS}/installer-iso/LOG-${architecture}.* ${NFS}/installer-iso/content ."
			    archiveArtifacts artifacts: "*.iso,LOG-*,content/**", fingerprint: true, onlyIfSuccessful: true
			}
		    }
                }
		post {
		    /* always {
		     ircNotify customMessage: "$BUILD_DISPLAY_NAME: finished binary build for ${architecture} on ${NODE_NAME}: ${currentBuild.currentResult}" 
		 } */
		    cleanup {
			echo "Cleaning architecture:${env.architecture} workspace on ${env.NODE_NAME}"
			deleteDir()
		    }
		}
	    }
        }

    }


    post {
	/* always {
	     ircNotify ()
	} */

	cleanup {
	    echo "Cleaning workspace on ${env.NODE_NAME}"
	    deleteDir()
	}
    }
}
