71F807                  0fWelcome to Devuan GNU/Linux!07

    Devuan ${DEBIAN_VERSION} installer built on ${BUILD_DATE}.






    0fSPECIAL BOOT PARAMETERS - INSTALLATION SYSTEM07

You can use the following boot parameters at the boot prompt, in combination
with the boot method (see <07F307>).

  0fRESULT                               PARAMETER TO SPECIFY07
* Disable framebuffer                  0fvga=normal fb=false07
* Don't start PCMCIA                   0fhw-detect/start_pcmcia=false07
* Force static network config          0fnetcfg/disable_dhcp=true07
* Set keyboard map                     0fbootkbd=es07
Accessibility options (options not available for all images):
* Use high contrast theme              0ftheme=dark07
* Use Braille tty                      0fbrltty=driver,device,texttable07
* Use Speakup                          0fspeakup.synth=driver07

Example: install vga=normal fb=false


Press F1 through F10 for more help or ENTER to return to the menu.
