71F207                  0fWelcome to Devuan GNU/Linux!07

    Devuan ${DEBIAN_VERSION} installer built on ${BUILD_DATE}.






    0fPREREQUISITES07

* You must have at least 0f800 MiB of RAM07 to use this Devuan installer.

* You also need to have disk space for the boot system, say 0f4 GiB07
  total, divided into say 0f0.5 GiB07 for EFI boot, 0f2.7 GiB07 root
  filesystem and 0f0.8 GiB07 swap. 

See the Installation Guide or the FAQ for more information. There is
an installation guide on this ISO. See also the Devuan web site,

                     0fhttps://www.devuan.org/07

Thank you for choosing Devuan.  Your journey to init freedom has begun!




Press F1 through F10 for more help or ENTER to return to the menu.
