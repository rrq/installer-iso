#!/usr/bin/env newlisp
#
# This script reviews a package list file, expanding it to include
# all dependencies, and documenting a "reason" for all the packages:
#   implied = dependency from some other package
#   virtual = dependency, but provided by some package
#   origin = not a dependency
#
# The review uses a package description library in ome or more directories
# named as library-* where each package description is a single file that
# is named by the package it describes.
#
# See library.lsp as a script that creates a library from given Packages
# files.
# [-r] <file> [ -- <package>... ]

(signal 2 (fn (x) (exit 0)))

(let (it)
  (if (setf it (match '(* "-r" * "--" *) (main-args)))
      (set 'USE_RECOMMENDS true 'LISTFILE (it 1 -1) 'ADDONS (it 2))
    (setf it (match '(* "-r" *) (main-args)))
    (set 'USE_RECOMMENDS true 'LISTFILE (it 1 -1) 'ADDONS '())
    (setf it (match '(* "--" *) (main-args)))
    (set 'USE_RECOMMENDS nil 'LISTFILE (it 0 -1) 'ADDONS (it 1))
    (setf it (main-args))
    (set 'USE_RECOMMENDS nil 'LISTFILE (it -1) 'ADDONS '())
    ))

(setf ARCH ((exec "dpkg-architecture -qDEB_HOST_ARCH") 0))

(unless (file? LISTFILE)
  (write-line 2 "Usage: [-r] <listfile> [ -- <package>* ]" )
  (exit 1))

(define DEPS:DEPS nil)
(define BASE:BASE nil)
(define PICK:PICK nil)
(define PROV:PROV nil)
(define SKIP:SKIP nil)
    
(define (base-name P)
  (replace "[0-9.]+" (copy P) "([0-9.]+)" 0))

(define (version P)
  (or (find-all "([0-9]+)" P (int $1 0 10) 0) 0))

; Higher or same version
(define (higher-version x y)
  (if (null? x) (null? y) (null? y) true
      (= (x 0) (y 0)) (higher-version (1 x) (1 y))
      (> (x 0) (y 0))))

(define (version-order x y)
  (higher-version (version x) (version y)))

(define (save-base B P)
  (BASE B (sort (cons P (or (BASE B) '())) version-order)))

(define (save-package P)
  (setf PKG P)
  (DEPS P (list)) ; for dependencies
  (when (find "[0-9]" P 0) ; Maybe has version numbers in name
    (save-base (base-name P) P)))

(define (save-provides DL)
  (setf DL (map trim (parse (replace "\\([^)]+\\)" DL "" 0) ",")))
  (dolist (D DL)
    (if (PROV D) (push PKG (PROV D) (if (= PKG D) 0 -1)) (PROV D (list PKG)))))

(define (save-depends DL)
  (setf DL
        (map (fn (D) (replace ":any$" D "" 0))
             (map trim (parse (replace "\\([^)]+\\)" DL "" 0) ","))))
  (if (DEPS PKG) (extend (DEPS PKG) DL) (DEPS PKG DL)))

(define (add-pick B P)
  (unless (member P (or (PICK B) '()))
    (if (PICK B) (push P (PICK B) -1) (PICK B (list P)))))

(write-line 2 (format "Loading (library|security)-%s-*/* files" ARCH))
(write-line 2 (string "USE_RECOMMENDS = " USE_RECOMMENDS))
(dolist (LIB (directory "." (format "^(library|security)-%s" ARCH)))
  (dolist (PKG (directory LIB "^[^.]"))
    (dolist (LINE (parse (read-file (format "%s/%s" LIB PKG)) "\n"))
      (when (regex "(^[^:]+): (.*)" LINE 0)
        (case $1
          ("Package" (save-package $2) (save-provides PKG))
          ("Provides" (save-provides $2))
          ("Depends" (save-depends $2))
          ("Pre-Depends" (save-depends $2))
          ("Recommends" (when USE_RECOMMENDS (save-depends $2)))
          (true ))))))

; Process provisions into alternative dependency, and remove all other.
(dolist (PX (PROV))
  (println (string "# provided " PX))
  (setf PKG (PX 0))
  (if (!= PKG (PX 1 -1))
      (save-depends (join (PX 1) " | "))
    (PROV PKG nil)))

; Load the SKIP table, if any
(dolist (f (filter file? (parse (or (env "SKIP") "") ":")))
  (dolist (P (clean (fn (x) (or (empty? x) (find "#" P)))
                    (parse (read-file f) "\n")))
    (unless (SKIP P) (SKIP P P))))

; Now process stdin package list

(setf ADDED '()) ; For tracking additions to IMPLIED

(define ORIG:ORIG nil)
(define IMPLIED:IMPLIED nil)
(define CHOOSEN:CHOOSEN nil)

(define (add-added X)
  (println (format "# add-added %s" (join (map string (args)))))
  (push X ADDED -1))

(define (add-implied P D)
  (println "# add-implied " P " " D)
  (if (list? D) nil
    (find "|" D) (add-added D "add-implied:1 " D)
    (member P (or (IMPLIED D) '())) nil
    (nil? (DEPS D)) (write-line 2 (string "# unavailable " D ))
    (begin (if (IMPLIED D) (push P (IMPLIED D) -1) (IMPLIED D (list P)))
           (add-added D "add-implied:2 " D))
    ))

(define (add-choice P R X)
  (println "# add-choice " P " " R " " X)
  (if (CHOOSEN P) (or (member R (CHOOSEN P)) (push R (CHOOSEN P) -1))
    (CHOOSEN P (list R)))
  (add-added P "add-choice " P)
  (map (curry add-implied P) (or (DEPS P) '())) P)

(define (analyse-simple P)
  (add-choice P P "analyse-simple"))

(define (analyse-base B P)
  (let ((BL (BASE B)))
    (if (null? BL) (println (string "# discard: " P " " B))
      (add-pick B P)
      (add-choice P P "analyse-base"))))

(write-line 2 "Processing")

(dolist (P (extend (parse (read-file LISTFILE) "\n") ADDONS))
  (unless (or (empty? P) (starts-with (trim P) "#") (SKIP P))
    (ORIG (trim P) P)
    (if (find "[0-9]" P 0) (analyse-base (base-name P) P)
      (nil? (DEPS P)) (println "# unknown: " P)
      (analyse-simple P))))

(define (has-provider P)
  (exists (fn (x) (and (!= x P) (CHOOSEN x))) (or (PROV P) '())))

# PL might be a list of alternatives to chose from; each alternative
# has provider(s) to investigate for any existing choice
(define (add-first-choice PL)
  (if (exists (fn (P) (CHOOSEN P)) PL) $it
    (if (filter has-provider PL) ($it 0)
      (if (exists (fn (x) (not (nil? (DEPS x)))) PL)
          (add-choice $it (join PL " | ")
                      (string "add-first-choice:" I))))))

# Process all implied packages for recursiveness
(define MANUAL:MANUAL nil)
(define (manual-choice PL BL)
  (if (MANUAL (string PL)) $it
    (begin
      (write-line 2 (string "Confirm choice " PL " for " BL))
      (dolist (P PL)
        (if (IMPLIED P) (write-line 2 (string P " is implied by " (IMPLIED P)))
          (write-line 2 (string P " is choosen"))))
      (let ((X (read-line)))
        (when (= "y" X) (setf X ""))
        (MANUAL (string PL) (if (empty? X) PL (parse X " ")))))))

(define (ssort X) (if (list? X) (sort X) X))

(while ADDED
  (while ADDED
    (letn ((I (pop ADDED))
           (P (add-first-choice
               (map (fn (x) (replace ":any$" x "" 0))
                    (map trim (parse I "|"))))))
      (when (or (find "|" I) (!= I P))
        (println (string "# choice " P " for " I " by " (ssort (IMPLIED I))))
        (when (and P (IMPLIED I))
          (map (curry add-implied P) (IMPLIED I))))))
  (dolist (PX (PICK))
    (letn ((B (PX 0)) (BL (BASE B)))
      (unless (= (PX 1 0) (BL 0))
        (let ((CL (manual-choice (PX 1) BL)))
          (dolist (P (difference (PX 1) CL)) (CHOOSEN P nil))
          (dolist (P CL)
            (unless (or (null? P) (CHOOSEN P))
              (add-added P "processing " P)))))))
  ) ; end while ADDED

# Now report any unfulfilled virtual dependencies

(println "##### Choosen ###########")

(define (print-result P)
  (println (join (map string (args)) " "))
  (unless (empty? P) (println P)))

; Process the CHOOSEN table with a one-liner comment as to why:
; 1. "# dropped" P is dropped because it's missing in the library
; 2. "# origin" P is an input and not implied
; 3. "# origin" P is an input and implied and has an alternative provider
; 4. "# shadowed" P is implied but has an alternative provider
; 5. "# choosen" P is implied and first choice among non-chosen other
; 6. "# implied" P is implied and without alternative provider
; Note that the package is then NOT included for cases 1 and 4, while
; it is included (printed on the subsequent line) for all other cases.
(dolist (PX (CHOOSEN))
  (let ((P (PX 0)) (PS nil) (it nil))
    (if (nil? (DEPS P)) (print-result "" "# dropped" P)
      (null? (IMPLIED P)) (print-result P "# origin" P)
      (PROV P)
      (if (setf it (exists (fn (x) (and (!= x P) (CHOOSEN x))) (PROV P)))
          (if (ORIG P)
              (print-result P "# origin" P "for" (ssort (PROV P))
                            "implied by" (ssort (IMPLIED P)))
            (print-result "" "# shadowed" P "by" it "for" (ssort (PROV P))
                          "implied" (ssort (IMPLIED))))
        (print-result P "# choosen" P "for"
                      (ssort (PROV P)) "supported by" (ssort (IMPLIED P))))
      (print-result P "# implied" P "by" (ssort (IMPLIED P))))))

(exit 0)
